function handleClick() {
    const h1Elem = document.getElementsByTagName('h1')[0];
    if (h1Elem.style.display === 'none') {
        h1Elem.style.display = 'block';
    } else {
        h1Elem.style.display = 'none';
    }
}

const btn = document.getElementsByTagName('button')[0];
btn.addEventListener('click', handleClick);
