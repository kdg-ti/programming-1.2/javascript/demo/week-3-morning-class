const bodyElement = document.getElementsByTagName('body')[0];

const divElement = document.createElement('div');
const pElement = document.createElement('p');
const emElements = [
    document.createElement('em'),
    document.createElement('em'),
];

// div > p
divElement.appendChild(pElement);
// div > p > em    (2 children)
for (let emEle of emElements) {
    //const textNode = document.createTextNode('This is some text');
    //emEle.appendChild(textNode);
    emEle.innerText = 'Inner text'
    pElement.appendChild(emEle);
}

bodyElement.appendChild(divElement);
/*bodyElement.replaceChild(
    divElement,
    document.getElementsByTagName('h1')[0]);*/

// NOT recommended!!!
divElement.setAttribute('style', 'color: yellow');
// There's getAttribute
// Style is quite handy and important
divElement.style.backgroundColor = 'red';

divElement.setAttribute('class', 'light'); // class="light" OVERWRITE
/* divElement.setAttribute(
    'class',
    divElement.getAttribute('class') + " light"); */

// Recommended: more flexible!!
divElement.classList.add('light'); // ADD 'light' to the list of classes
divElement.classList.remove('dark');

// Careful!!! (as soon as we're using events)
// Sometimes NOT recommended
// document.getElementsByTagName('p')[1].innerHTML
//     = 'sdfsdf <em>sdfsdsdf</em>';
