const sectionElement = document.getElementById('mainSection');
const bodyElement = sectionElement.parentElement;
console.log('BODY \'Name\': ' + bodyElement.nodeName);

let index = 0;
for (let elem of bodyElement.children) { // 'children' --> elements
    index++;
    console.log(`Child element ${index}: ${elem.nodeName}`);
}

index = 0;
for (let node of bodyElement.childNodes) { // 'childNodes' --> Nodes
    index++;
    console.log(`Child node ${index}: ${node.nodeName}`);
}

const pElements = document.getElementsByTagName('p'); // getElementsByTagName
console.log('All paragraphs:');
console.log(pElements);


const darkElements = document.getElementsByClassName('dark'); // getElementsByClassName
console.log('All dark elements:');
for (let elem of darkElements) {
    console.log('Elem: ' + elem.nodeName);
}

const element = document
    .querySelector('#mainSection > p.dark'); // 1 element
const elements = document
    .querySelectorAll('#mainSection > p.dark'); // Multiple

const a = document.getElementsByTagName('h2'); // ALL h2 within the document
const b = sectionElement.getElementsByTagName('h2'); // All h2 within the section
const c = sectionElement.querySelector('.dark'); // Relative to the section
